/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.mavenproject1;

/**
 *
 * @author Darpan Subedi
 */
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
 
@ManagedBean(name="student")
@SessionScoped
public class StudentBean implements Serializable{

    private static final long serialVersionUID = 1L;

    String fname, mname, lname, faculty, program;
// Getter and setter

    public String getFname() {
        return fname;
    }
    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }
    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }
    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFaculty() {  
        return faculty;  
    }  
    public void setFaculty(String faculty) {  
        this.faculty = faculty;  
    }
  
    public String getProgram() {
        return program;
    }
    public void setProgram(String program) {
        this.program = program;
    }
     
    private static final ArrayList<Student> studentList = new ArrayList<Student>();

    public ArrayList<Student> getStudentList() {
        return studentList;
    }

    public String addAction() {
        Student student = new Student(this.fname, this.mname, this.lname, this.faculty, this.program);
		
        studentList.add(student);	
	return null;
	}
 
    public String deleteAction(Student student) {
	studentList.remove(student);
	return null;
	}
        
    public String saveAction() {
	//get all existing value but set "editable" to false
	for (Student student : studentList){
            student.setEditable(false);
	}
	//return to current page
	return null;
        }

        public String editAction(Student student) {
	student.setEditable(true);
	return null;
    }

public static class Student{
		
	String fname, mname, lname, faculty, program;
        boolean editable;


	public Student(String fname,String mname,String lname,String faculty,String program) {
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.faculty = faculty;
                this.program = program;
	}

                public boolean isEditable(){
                        return editable;
                }
                public void setEditable(boolean editable) {
                        this.editable = editable;
                
                }
    public String getFname() {
        return fname;
}
                    
    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
       

    public String getFaculty() {  
        return faculty;  
    }  
    public void setFaculty(String faculty) {  
        this.faculty = faculty;  
    }  
    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
        }
		
    }
}
